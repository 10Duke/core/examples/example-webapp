# Example frontend

This project implements a 'My licenses' app as a client side, single page browser application.

It is built using [Typescript](https://www.typescriptlang.org/), [Vue JS](https://vuejs.org/),
[Vite](https://vitejs.dev/), and [pinia](https://pinia.vuejs.org/).

It talks to the 10Duke API via a web API built on top of a 10Duke SDK.

The application exists to be used with a variety of API implementations to test the different
language bindings for the 10Duke SDKs. The project covers the element labeled "Frontend"
in the diagram below.

```mermaid
flowchart LR
    Frontend-->Backend
    subgraph Backend
        App["app logic"] -->SDK
    end
    Backend-->ScaleAPI
    style Frontend fill:#696
```

## Project setup

```bash
yarn global add @vue/cli
yarn global add @vue/cli-service
yarn install
```

## Configuration

Copy [`.env.example`](./.env.example) to `.env`.

Edit the file and set values for the API host URL and the OIDC provider sign up page.

**N.B.** See the documentation for the particular backend project for the API host URL and to add
required configuration that is dependent on this project. Dependent on how the frontend is being run
(development server vs. built application as static files) the backend may need to be configured
with the URL the frontend is running on.

## Build and run

The following commands are defined in the [`package.json`](./package.json) file.

### Compiles and hot-reloads for development

```bash
yarn dev
```

### Compiles and minifies for production

```bash
yarn build
```

### Lints and fixes files

```bash
yarn lint
```
