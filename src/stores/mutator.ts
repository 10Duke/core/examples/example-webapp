import { Ref } from 'vue'

const isArrayPopulated = <T, > (refHolder: Ref<Array<T>>): boolean =>{
  return Array.isArray(refHolder.value) && refHolder.value.length !== 0
}

const mutateRefIfArrayPopulated = <T, >(refHolder: Ref<Array<T>>, newValue: Array<T>) => {
  if (Array.isArray(newValue))
  {
    refHolder.value = newValue
  } else {
    refHolder.value = []
  }
}

const mutateRefIfNotUndefined = <T, >(refHolder: Ref<T | null>, newValue: T | null | undefined ) => {
  if (newValue) {
    refHolder.value = newValue
  } else {
    refHolder.value = null
  }
}

export { isArrayPopulated, mutateRefIfArrayPopulated, mutateRefIfNotUndefined }
