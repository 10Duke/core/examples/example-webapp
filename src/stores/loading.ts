import { computed, ref } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {

  const backing = ref([false])

  const isLoading = computed(() => backing.value[backing.value.length - 1])

  const setLoading = () => {
    backing.value.push(true)
  }

  const unsetLoading = () => {
    if (backing.value[backing.value.length - 1]) {
      backing.value.pop()
    }
  }

  return { isLoading, setLoading, unsetLoading }
})
