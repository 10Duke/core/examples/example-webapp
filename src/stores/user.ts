import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import {
  getLoginUrl as getLoginUrlHttp,
  login as loginHttp,
  logout as logoutHttp
} from '@/api/http'
import { UserInfo } from '@/api/types'

export const useUserStore = defineStore('user', () => {
  const isLoggedIn = ref(false)

  const user = ref(null as UserInfo | null)

  const displayName = computed(() => {
    if (!isLoggedIn.value) {
      return ''
    }
    if (user.value?.name) {
      return user.value?.name
    }
    const name = `${user.value?.givenName} ${user.value?.familyName}`
    if (name.trim().length === 0) {
      return user.value?.email
    }
    return name
  })

  const getLoginUrl = async (): Promise<string> => {
    const url= await getLoginUrlHttp()
    return url
  }

  const login = async (queryString: string) => {
    const loggedInUser = await loginHttp(queryString)
    isLoggedIn.value = true
    user.value = loggedInUser
  }

  const logout = async () => {
    await logoutHttp()
    isLoggedIn.value = false
    user.value = null
  }

  return { isLoggedIn, user, displayName, getLoginUrl, login, logout }
},
{
  persist: true,
})
