import { ref } from 'vue'
import { defineStore } from 'pinia'
import { Licensee } from '@/api/types'
import { getLicensees }  from '@/api/http'
import { mutateRefIfNotUndefined } from '@/stores/mutator'
import { useUserStore } from './user'

export const useLicenseeStore = defineStore('licensee', () => {
  const isLoaded = ref(false)
  const selectedLicenseeId = ref('' as string)
  const licensees = ref([] as Array<Licensee> | null)
  const selected = ref(null as Licensee | null)

  const loadLicensees = async () => {
    const licenseesModels = await getLicensees()
    if (Array.isArray(licenseesModels) && licenseesModels.length != 0) {
      licensees.value = licenseesModels
      selected.value = licenseesModels[0]
      selectedLicenseeId.value = selected.value.id
      isLoaded.value = true
    }
  }

  const setSelectedLicensee = (id: string) => {
    selectedLicenseeId.value = id
    const maybe = licensees.value?.find(x => x.id === selectedLicenseeId.value)
    mutateRefIfNotUndefined(selected, maybe)
  }

  const userStore = useUserStore()

  userStore.$subscribe(() => {
    if (!userStore.isLoggedIn) {
      licensees.value = []
      selectedLicenseeId.value = ''
      selected.value = null
    }
  })

  return { isLoaded, licensees, loadLicensees, setSelectedLicensee, selectedLicenseeId, selected }
})
