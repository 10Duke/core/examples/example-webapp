import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLicenseeStore } from './licensee'
import { MenuItem } from 'primevue/menuitem'

export const useMenuStore = defineStore('menu', () => {
  const licenseeStore = useLicenseeStore ()
  const logoutItem : MenuItem = {
    label: 'Logout',
    icon: 'pi pi-signout',
    url: '/logout'
  }
  const isLoaded= ref(false)
  const items = ref([
    logoutItem,
  ])

  const buildMenuItems = (id: string): Array<MenuItem> => {
    let menuItems = [] as Array<MenuItem>

    if (Array.isArray(licenseeStore.licensees) && licenseeStore.licensees.length != 0)
    {
      const mappedItems = licenseeStore.licensees.map(
        (x) => (
          {
            label: x.isPerson ? 'Personal licenses' : x.name,
            icon: x.id === id ? 'pi pi-check' : '',
            command: () => selectLicensee(x.id),
          } as MenuItem
        )
      )
      menuItems = [...menuItems, ...mappedItems]
    }
    menuItems.push(logoutItem)
    return menuItems
  }

  const loadMenuItems = async () => {
    await licenseeStore.loadLicensees()

    items.value = buildMenuItems(licenseeStore.selectedLicenseeId!)

    isLoaded.value = true
  }

  const selectLicensee = (id: string) => {
    licenseeStore.setSelectedLicensee(id)
    items.value = buildMenuItems(id)
  }

  return { isLoaded, items, loadMenuItems, selectLicensee }
})
