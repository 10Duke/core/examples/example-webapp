import { ref } from 'vue'
import { defineStore } from 'pinia'
import { Product } from '@/api/types'
import { getProducts } from '@/api/http'
import { mutateRefIfArrayPopulated } from './mutator'

export const useProductStore = defineStore('product', () => {
  const products = ref([] as Array<Product>)
  const isLoading = ref(false)
  const loaded = ref(false)

  const getProductsForLicensee = async (id: string) => {
    isLoading.value = true
    if (id) {
      var apiModels = await getProducts(id)
      mutateRefIfArrayPopulated(products, apiModels)
    }
    isLoading.value = false
    loaded.value = true
  }

  return { isLoading, loaded, loadProducts: getProductsForLicensee, products }
})
