import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLicenseeStore } from './licensee'
import { License } from '@/api/types'
import { checkout as httpCheckout, getLicenses as getLicensesHttp, getCheckouts as getCheckoutsHttp } from '@/api/http'
import { isArrayPopulated, mutateRefIfArrayPopulated } from './mutator'

export const useLicenseStore = defineStore('license', () => {
  const licenseeStore = useLicenseeStore()
  const licenses = ref([] as Array<License>)
  const allLicensesLoaded = ref(false)
  const isLoadingLicenses = ref(false)
  const isLoadingCheckouts = ref(false)
  const licensesLoaded = ref(false)

  const getLicenses = async (selectedLicenseeId: string) => {
    if (selectedLicenseeId) {
      isLoadingLicenses.value = true
      var result = await getLicensesHttp(selectedLicenseeId, 0)
      allLicensesLoaded.value = result.allLicensesLoaded
      mutateRefIfArrayPopulated(licenses, result.licenses)
      isLoadingLicenses.value = false
      licensesLoaded.value = true
    }
  }

  const getMoreLicenses = async (selectedLicenseeId: string) => {
    if (licenseeStore.selectedLicenseeId) {
      isLoadingLicenses.value = true
      const offset = isArrayPopulated(licenses) ? licenses.value.length : 0
      var result = await getLicensesHttp(selectedLicenseeId, offset)
      allLicensesLoaded.value = result.allLicensesLoaded
      licenses.value = [...licenses.value, ...result.licenses]
      isLoadingLicenses.value = false
      licensesLoaded.value = true
    }
  }

  const checkout = async (licenseeId: string, productName: string, id: string) : Promise<string> => {
    const token = await httpCheckout(productName, id)
    await getCheckouts(licenseeId, id)
    return token
  }

  const getCheckouts = async (licenseeId: string, licenseId: string) => {
    isLoadingCheckouts.value = true
    const license = licenses.value.find(x => x.id === licenseId)
    if (license) {
      const checkouts = await getCheckoutsHttp(licenseeId, licenseId, license.checkouts.length)
      license.checkouts = [...license.checkouts, ...checkouts.checkouts]
      license.allCheckoutsLoaded = checkouts.allCheckoutsLoaded
    }
    isLoadingCheckouts.value = false
  }

  const getAllCheckouts = async(licenseeId: string, licenseId: string) => {
    isLoadingCheckouts.value = true
    const license = licenses.value.find(x => x.id === licenseId)
    if (license) {
      const checkouts = await getCheckoutsHttp(licenseeId, licenseId, 0)
      license.checkouts = checkouts.checkouts
      license.allCheckoutsLoaded = checkouts.allCheckoutsLoaded
    }
    isLoadingCheckouts.value = false
  }

  const resetCheckouts = () => {
    licenses.value.forEach(x => {
      x.checkouts = []
      x.allCheckoutsLoaded = false
    })
  }

  return {
    allLicensesLoaded,
    checkout,
    getAllCheckouts,
    getCheckouts,
    getLicenses,
    getMoreLicenses,
    isLoadingCheckouts,
    isLoadingLicenses,
    licenses,
    licensesLoaded,
    resetCheckouts,
  }
})
