export interface CheckoutDetails {
  created: Date,
  clientHardwareId: string,
  validFrom: Date,
  validTo?: Date,
}

export interface CheckoutsResponse {
  allCheckoutsLoaded: boolean,
  checkouts: Array<CheckoutDetails>,
}

export interface License {
  id: string,
  productName: string,
  validFrom: Date,
  validTo?: Date,
  licenseeId: string,
  allCheckoutsLoaded: boolean,
  checkouts: Array<CheckoutDetails>,
}

export interface Licensee {
  id: string,
  name: string,
  description: string,
  isPerson: boolean,
}

export interface LicensesResult {
  licenses: Array<License>,
  allLicensesLoaded: boolean,
}

export interface Product {
  productName: string,
  features: Array<string>,
}

export interface UserInfo {
  name: string,
  email: string,
  givenName: string,
  familyName: string,
}
