export interface ErrorBody {
  code: number,
  error: string,
  description: string,
}

export class ApiError extends Error {
  public statusCode: number
  public error?: string
  public description?: string

  constructor(msg: string, statusCode: number, errorBody?: ErrorBody) {
    super(msg)
    this.statusCode = errorBody?.code ?? statusCode
    this.error = errorBody?.error
    this.description = errorBody?.description
    Object.setPrototypeOf(this, ApiError.prototype)
  }
}

export const getErrorMessage = (error: unknown): string => {
  if (error instanceof ApiError) {
    return `${error.statusCode} - ${error.description ?? error.message}`
  }
  if (error instanceof Error) return error.message
  return String(error)
}
