import { CheckoutsResponse,Licensee, LicensesResult, Product, UserInfo } from '@/api/types'
import { useLoadingStore } from '@/stores/loading'
import { ApiError } from '@/api/apierror'

type Fetcher = () => Promise<Response>
type ResponseProcessor = (res: Response) => Promise<any>

const fetchHandler = async (fetcher: Fetcher, responseProcessor: ResponseProcessor): Promise<any> => {
  const store = useLoadingStore()
  store.setLoading()
  try {
    const res = await fetcher()
    if (res.ok){
      const result = await responseProcessor(res)
      return result
    }
    else {
      const body = await res.json()
      throw new ApiError(res.statusText, res.status, body)
    }
  }
  finally {
    store.unsetLoading()
  }
}

const buildGetFetcher = (url: string): Fetcher => {
  return async () => await fetch(url, { mode: 'cors', redirect: 'follow', credentials: 'include' })
}

const getJsonResponseProcessor = async (res: Response) => {
  return await res.json()
}

const getRequestJson = async (url: string): Promise<any> => {
  const fetcher = buildGetFetcher(url)
  return fetchHandler(fetcher, getJsonResponseProcessor)
}

const getTextResponseProcessor = async (res: Response) => {
  return await res.text()
}

const getRequestText = async (url: string): Promise<any> => {
  const fetcher = buildGetFetcher(url)
  return fetchHandler(fetcher, getTextResponseProcessor)
}

const buildPostFetcher = (url: string, body: any): Fetcher => {
  return async () => await fetch(
    url,
    {
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
      },
      redirect: 'follow',
      body: JSON.stringify(body),
    }
  )
}

const postRequestJson = async (url: string, body: any): Promise<any> => {
  const fetcher = buildPostFetcher(url, body)
  return fetchHandler(fetcher, getJsonResponseProcessor)
}

const checkout = async (productName: string, id: string): Promise<string> => {
  const checkoutUrl = `${import.meta.env.VITE_API_URL}/licenses/${productName}/${id}/checkout`
  return await getRequestText(checkoutUrl)
}

const getCheckouts = async (
  licenseeId: string,
  licenseId: string,
  offset: number
): Promise<CheckoutsResponse> => {
  const getCheckoutsUrl = `${import.meta.env.VITE_API_URL}/licensees/${licenseeId}/licenses/${licenseId}/checkouts?offset=${offset}`
  return await getRequestJson(getCheckoutsUrl)
}

const getLicensees = async (): Promise<Array<Licensee>> => {
  const getLicenseesEndpoint = `${import.meta.env.VITE_API_URL}/licensees`
  return await getRequestJson(getLicenseesEndpoint)
}

const getLicenses = async (licenseeId: string, offset: number): Promise<LicensesResult> => {
  const getLicensesEndpoint = `${import.meta.env.VITE_API_URL}/licensees/${licenseeId}/licenses?offset=${offset}`
  return await getRequestJson(getLicensesEndpoint)
}

const getLoginUrl = async (): Promise<string> => {
  const loginUrlEndpoint = `${import.meta.env.VITE_API_URL}/auth/loginurl`
  return await getRequestText(loginUrlEndpoint)
}

const getProducts = async (licenseeId: string): Promise<Array<Product>> => {
  const productsUrl = `${import.meta.env.VITE_API_URL}/licensees/${licenseeId}/products`
  return await getRequestJson(productsUrl)
}

const login = async (queryString: string): Promise<UserInfo> => {
  const loginEndpoint = `${import.meta.env.VITE_API_URL}/auth/login`
  return await postRequestJson(loginEndpoint, { data: queryString })
}

const logout = async () => {
  const logoutEndpoint = `${import.meta.env.VITE_API_URL}/auth/logout`
  await fetch(
    logoutEndpoint,
    {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'include',
    }
  )
}

const getSignUpUrl = (): string => import.meta.env.VITE_SIGNUP_URL

export {
  checkout,
  getCheckouts,
  getLicenses,
  getLicensees,
  getLoginUrl,
  getProducts,
  getSignUpUrl,
  login,
  logout,
}
