import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'
import PrimeVue from "primevue/config";
import { usePassThrough } from "primevue/passthrough"
import Tailwind from "primevue/passthrough/tailwind"
import ToastService from 'primevue/toastservice'
import App from './App.vue'
import router from './router'
import './style.css'

const app = createApp(App)

const pinia = createPinia()
pinia.use(piniaPluginPersistedState)

app.use(pinia)
app.use(router)
app.use(ToastService)

const TRANSITIONS = {
  overlay: {
    class: 'bg-themeblue-500 dark:bg-themeblue-500',
    enterFromClass: 'opacity-0 scale-75',
    enterActiveClass: 'transition-transform transition-opacity duration-150 ease-in',
    leaveActiveClass: 'transition-opacity duration-150 ease-linear',
    leaveToClass: 'opacity-0'
  }
}

const CustomTailwind = usePassThrough(
  Tailwind,
  {
    avatar: {
      root: ({ props, parent }: any) => ({
        class: [
          'flex items-center justify-center',
          'bg-themeblue-500 dark:bg-themeblue-500 hover:bg-themeblue-600 text-slate-50 dark:text-slate-50',
          {
            'rounded-lg': props.shape == 'square',
            'rounded-full': props.shape == 'circle'
          },
          {
            'text-base h-8 w-8': props.size == null || props.size == 'normal',
            'w-12 h-12 text-xl': props.size == 'large',
            'w-16 h-16 text-2xl': props.size == 'xlarge'
          },
          {
            '-ml-4 border-2 border-white dark:border-gray-900': parent.instance.$css !== undefined
          }
        ]
      }),
      image: 'h-full w-full'
    },
    menu: {
      root: { class: ['py-1 bg-themeblue-500 dark:bg-themeblue-500 text-slate-50 dark:text-slate-50 border border-blue-900/40 dark:border-blue-900/40 rounded-md w-48']},
      menu: {
        class: ['m-0 p-0 list-none', 'outline-none']
      },
      content: ({ context }: any) => ({
        class: [
          'text-slate-50 dark:text-slate-50 bg-themeblue-500 dark:bg-themeblue-500 transition-shadow duration-200 rounded-none',
          'hover:text-slate-200 dark:hover:text-slate-200 hover:bg-themeblue-600 dark:hover:bg-themeblue-600', // Hover
          {
            'bg-themeblue-500 dark:bg-themeblue-500 text-slate-50 dark:text-slate-50': context.focused,
          },
        ],
      }),
      action: {
        class: ['text-slate-50 dark:text-slate-50 py-3 px-5 select-none', 'cursor-pointer flex items-center no-underline overflow-hidden relative'],
      },
      icon: 'text-slate-50 dark:text-slate-50 mr-2',
      submenuheader: {
        class: ['m-0 p-3 text-slate-50 dark:text-slate-50 bg-themeblue-500 dark:bg-themeblue-500 font-bold rounded-tl-none rounded-tr-none'],
      },
      transition: TRANSITIONS.overlay,
    },
    fileupload: {
      choosebutton: {
        class: [
          'inline-flex align-center p-1 px-5 rounded-md text-base overflow-hidden',
          'text-slate-50 dark:text-slate-50 bg-themeblue-500 dark:bg-themeblue-500 transition-shadow duration-200 ease-in-out rounded',
          'hover:text-slate-50 dark:hover:text-slate-50 hover:bg-themeblue-600 dark:hover:bg-themeblue-600 cursor-pointer',
          'border border-themeblue-500 dark:border-themeblue-500',
          'focus:shadow-[0_0_0_2px_rgba(255,255,255,1),0_0_0_4px_rgba(157,193,251,1),0_1px_2px_0_rgba(0,0,0,1)] dark:focus:shadow-[0_0_0_2px_rgba(28,33,39,1),0_0_0_4px_rgba(147,197,253,0.7),0_1px_2px_0_rgba(0,0,0,0)]',
          'focus:outline-none focus:outline-offset-0 ',
        ],
      },
      uploadicon: { class: ['mt-1 mr-2'] },
    },
    button: {
      root: {
        class: [
          'bg-themeblue-500 dark:bg-themeblue-500 text-slate-50 dark:text-slate-50',
          'border border-themeblue-500 dark:border-themeblue-500',
          'hover:bg-themeblue-600 dark:hover:bg-themeblue-600 hover:border-themeblue-600 dark:hover:border-themeblue-600',
          'focus:shadow-[0_0_0_2px_rgba(255,255,255,1),0_0_0_4px_rgba(157,193,251,1),0_1px_2px_0_rgba(0,0,0,1)] dark:focus:shadow-[0_0_0_2px_rgba(28,33,39,1),0_0_0_4px_rgba(147,197,253,0.7),0_1px_2px_0_rgba(0,0,0,0)]',
          'items-center cursor-pointer inline-flex overflow-hidden relative select-none text-center align-bottom transition duration-200 ease-in-out focus:outline-none focus:outline-offset-0 rounded-md text-xs py-2 px-3'
        ],
      }
    },
  },
  {
    mergeSections: true,
    mergeProps: false,
  }
)

app.use(PrimeVue, { unstyled: true, pt: CustomTailwind })

app.mount('#app')
