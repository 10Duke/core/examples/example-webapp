#!/bin/sh

ROOT_DIR=/app

# Replace env vars in files served by NGINX
for file in $ROOT_DIR/assets/*.js* $ROOT_DIR/index.html $ROOT_DIR/precache-manifest*.js;
do
  sed -i 's|PLACEHOLDER_VITE_API_URL|'${VITE_API_URL}'|g' $file
  sed -i 's|PLACEHOLDER_VITE_SIGNUP_URL|'${VITE_SIGNUP_URL}'|g' $file
done
# Let container execution proceed
exec "$@"